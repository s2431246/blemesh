/**
 * These arguments were used when this file was generated. They will be automatically applied on subsequent loads
 * via the GUI or CLI. Run CLI with '--help' for additional information on how to override these arguments.
 * @cliArgs --board "/ti/boards/LP_CC2652RB" --product "simplelink_cc13x2_26x2_sdk@5.20.00.52"
 * @versions {"data":"2021060817","timestamp":"2021060817","tool":"1.8.2+1992","templates":null}
 */

/**
 * Import the modules used in this configuration.
 */
const ble         = scripting.addModule("/ti/ble5stack/ble");
const CCFG        = scripting.addModule("/ti/devices/CCFG");
const rfdesign    = scripting.addModule("/ti/devices/radioconfig/rfdesign");
const Display     = scripting.addModule("/ti/display/Display");
const Display1    = Display.addInstance();
const AESCCM      = scripting.addModule("/ti/drivers/AESCCM");
const AESCCM1     = AESCCM.addInstance();
const AESCTRDRBG  = scripting.addModule("/ti/drivers/AESCTRDRBG");
const AESCTRDRBG1 = AESCTRDRBG.addInstance();
const AESCTRDRBG2 = AESCTRDRBG.addInstance();
const AESECB      = scripting.addModule("/ti/drivers/AESECB");
const AESECB1     = AESECB.addInstance();
const ECDH        = scripting.addModule("/ti/drivers/ECDH");
const ECDH1       = ECDH.addInstance();
const GPIO        = scripting.addModule("/ti/drivers/GPIO");
const GPIO1       = GPIO.addInstance();
const GPIO2       = GPIO.addInstance();
const GPIO3       = GPIO.addInstance();
const GPIO4       = GPIO.addInstance();
const NVS         = scripting.addModule("/ti/drivers/NVS");
const NVS1        = NVS.addInstance();
const Power       = scripting.addModule("/ti/drivers/Power");
const RF          = scripting.addModule("/ti/drivers/RF");
const RTOS        = scripting.addModule("/ti/drivers/RTOS");
const TRNG        = scripting.addModule("/ti/drivers/TRNG");
const TRNG1       = TRNG.addInstance();

/**
 * Write custom configuration values to the imported modules.
 */
ble.lockProject                                  = true;
ble.disDevBySerUuid                              = true;
ble.addressMode                                  = "ADDRMODE_PUBLIC";
ble.scanType                                     = "SCAN_TYPE_PASSIVE";
ble.maxNumIcallEnabledTasks                      = 5;
ble.maxConnNum                                   = 4;
ble.deviceName                                   = "Simple Mesh and SP";
ble.maxPDUSize                                   = 255;
ble.mesh                                         = true;
ble.uuid                                         = system.utils.bigInt("53696D706C65104DA57368204E6F6465",16);
ble.completeCbName                               = "prov_complete_cbk";
ble.resetCbName                                  = "prov_reset_cbk";
ble.staticProv                                   = true;
ble.deviceOwnAddress                             = 2;
ble.radioConfig.codeExportConfig.$name           = "ti_devices_radioconfig_code_export_param0";
ble.connUpdateParamsPeripheral.$name             = "ti_ble5stack_general_ble_conn_update_params0";
ble.Element.create(2);
ble.Element[0].$name                             = "element_0";
ble.Element[0].model.create(15);
ble.Element[0].model[0].$name                    = "Model_0";
ble.Element[0].model[0].modelType                = "Vendor";
ble.Element[0].model[0].numOfHandlers            = 3;
ble.Element[0].model[0].handler1.$name           = "ti_ble5stack_mesh_dcd_handlers0";
ble.Element[0].model[0].handler1.handlerOpCode   = 0x00;
ble.Element[0].model[0].handler1.handlerFunction = "vnd_button_pressed_cb";
ble.Element[0].model[0].handler2.$name           = "ti_ble5stack_mesh_dcd_handlers1";
ble.Element[0].model[0].handler2.handlerOpCode   = 0x01;
ble.Element[0].model[0].handler2.handlerFunction = "vnd_get_status_cb";
ble.Element[0].model[0].handler3.$name           = "ti_ble5stack_mesh_dcd_handlers2";
ble.Element[0].model[0].handler3.handlerOpCode   = 0x02;
ble.Element[0].model[0].handler3.handlerFunction = "vnd_notify_status_cb";
ble.Element[0].model[1].$name                    = "Model_1";
ble.Element[0].model[2].$name                    = "Model_2";
ble.Element[0].model[2].sigModelName             = "GEN_ONOFF_CLI";
ble.Element[0].model[3].$name                    = "Model_3";
ble.Element[0].model[3].sigModelName             = "GEN_LEVEL_SRV";
ble.Element[0].model[4].$name                    = "Model_4";
ble.Element[0].model[4].sigModelName             = "GEN_LEVEL_CLI";
ble.Element[0].model[5].$name                    = "Model_5";
ble.Element[0].model[5].sigModelName             = "GEN_DEF_TRANS_TIME_SRV";
ble.Element[0].model[6].$name                    = "Model_6";
ble.Element[0].model[6].sigModelName             = "GEN_DEF_TRANS_TIME_CLI";
ble.Element[0].model[7].$name                    = "Model_7";
ble.Element[0].model[7].sigModelName             = "GEN_POWER_ONOFF_SRV";
ble.Element[0].model[8].$name                    = "Model_8";
ble.Element[0].model[8].sigModelName             = "GEN_POWER_ONOFF_CLI";
ble.Element[0].model[9].$name                    = "Model_9";
ble.Element[0].model[9].sigModelName             = "GEN_POWER_ONOFF_SETUP_SRV";
ble.Element[0].model[10].$name                   = "Model_10";
ble.Element[0].model[10].sigModelName            = "GEN_BATTERY_SRV";
ble.Element[0].model[11].$name                   = "Model_11";
ble.Element[0].model[11].sigModelName            = "GEN_BATTERY_CLI";
ble.Element[0].model[12].$name                   = "Model_12";
ble.Element[0].model[12].sigModelName            = "SENSOR_SRV";
ble.Element[0].model[13].$name                   = "Model_13";
ble.Element[0].model[13].sigModelName            = "SENSOR_CLI";
ble.Element[0].model[14].$name                   = "Model_14";
ble.Element[0].model[14].sigModelName            = "SENSOR_SETUP_SRV";
ble.Element[1].$name                             = "element_1";
ble.Element[1].model.create(1);
ble.Element[1].model[0].$name                    = "Model_15";
ble.Element[1].model[0].vendorID                 = 0x1234;
ble.Element[1].model[0].numOfHandlers            = 1;
ble.Element[1].model[0].companyVendorID          = 0xFFFF;
ble.Element[1].model[0].modelType                = "Vendor";
ble.Element[1].model[0].handler1.$name           = "ti_ble5stack_mesh_dcd_handlers3";
ble.bleTamplates.$name                           = "ti_ble5stack_mesh_ble_mesh_templates0";

CCFG.enableBootloader         = true;
CCFG.enableBootloaderBackdoor = true;
CCFG.dioBootloaderBackdoor    = 13;
CCFG.levelBootloaderBackdoor  = "Active low";
CCFG.ccfgTemplate.$name       = "ti_devices_CCFGTemplate0";

Display1.$name                          = "CONFIG_Display_0";
Display1.$hardware                      = system.deviceData.board.components.XDS110UART;
Display1.enableANSI                     = true;
Display1.uartBufferSize                 = 128;
Display1.uart.$name                     = "CONFIG_DISPLAY_UART";
Display1.uart.txPinInstance.outputState = "High";
Display1.uart.txPinInstance.$name       = "CONFIG_PIN_UART_TX";
Display1.uart.rxPinInstance.$name       = "CONFIG_PIN_UART_RX";

AESCCM1.$name = "CONFIG_AESCCM0";

AESCTRDRBG1.$name              = "CONFIG_AESCTRDRBG_0";
AESCTRDRBG1.aesctrObject.$name = "CONFIG_AESCTR_0";

AESCTRDRBG2.$name              = "CONFIG_AESCTRDRBG_1";
AESCTRDRBG2.aesctrObject.$name = "CONFIG_AESCTR_1";

AESECB1.$name = "CONFIG_AESECB0";

ECDH1.$name = "CONFIG_ECDH0";

GPIO1.$hardware         = system.deviceData.board.components["BTN-1"];
GPIO1.mode              = "Dynamic";
GPIO1.$name             = "CONFIG_GPIO_BTN1";
GPIO1.pinInstance.$name = "CONFIG_PIN_BTN1";

GPIO2.$hardware         = system.deviceData.board.components["BTN-2"];
GPIO2.mode              = "Dynamic";
GPIO2.$name             = "CONFIG_GPIO_BTN2";
GPIO2.pinInstance.$name = "CONFIG_PIN_BTN2";

GPIO3.$name             = "CONFIG_GPIO_0";
GPIO3.$hardware         = system.deviceData.board.components.LED_RED;
GPIO3.pinInstance.$name = "CONFIG_PIN_0";

GPIO4.$name             = "CONFIG_GPIO_1";
GPIO4.$hardware         = system.deviceData.board.components.LED_GREEN;
GPIO4.pinInstance.$name = "CONFIG_PIN_1";

NVS1.$name                    = "CONFIG_NVSINTERNAL";
NVS1.internalFlash.$name      = "ti_drivers_nvs_NVSCC26XX0";
NVS1.internalFlash.regionBase = 0x48000;
NVS1.internalFlash.regionSize = 0x4000;

TRNG1.$name = "CONFIG_TRNG_0";

/**
 * Pinmux solution for unlocked pins/peripherals. This ensures that minor changes to the automatic solver in a future
 * version of the tool will not impact the pinmux you originally saw.  These lines can be completely deleted in order to
 * re-solve from scratch.
 */
Display1.uart.uart.$suggestSolution       = "UART1";
Display1.uart.uart.txPin.$suggestSolution = "boosterpack.4";
Display1.uart.uart.rxPin.$suggestSolution = "boosterpack.3";
GPIO1.gpioPin.$suggestSolution            = "boosterpack.13";
GPIO2.gpioPin.$suggestSolution            = "boosterpack.12";
GPIO3.gpioPin.$suggestSolution            = "boosterpack.39";
GPIO4.gpioPin.$suggestSolution            = "boosterpack.40";
